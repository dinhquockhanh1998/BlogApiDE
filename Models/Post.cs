﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BlogApiDE.Models
{
    public class Post
    {
        public Post()
        {
            PostTags = new HashSet<PostTag>();
        }

        public int Id { get; set; }

        public int CategoryId { get; set; }

        public string Title { get; set; }

        public string ShortDescription { get; set; }

        public string FullDescription { get; set; }

        public bool Published { get; set; }

        public DateTime AddedDate { get; set; }

        public string AddedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        public string ModifiedBy { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        public virtual User User { get; set; }

        public virtual ICollection<PostTag> PostTags { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
    }
}
