﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BlogApiDE.Models
{
    public class Comment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CommentId { get; set; }

        public string Content { get; set; }
       
        public int PostId { get; set; }

        public string AddedBy { get; set; }

        public DateTime AddDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        [ForeignKey("PostId")]
        public virtual Post Post { get; set; }
       
    }
}
