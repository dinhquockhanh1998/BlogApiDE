﻿using BlogApiDE.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogApiDE.Data.Configurations
{
    public class PostConfiguration : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.HasKey(o => o.Id);

            builder.Property(o => o.Id).UseSqlServerIdentityColumn();

            builder.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(255);

            builder.Property(e => e.FullDescription).HasColumnType("ntext");

            builder
                .HasOne(o => o.Category)
                .WithMany(o => o.Posts);

        }
    }
}
