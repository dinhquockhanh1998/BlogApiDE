﻿using BlogApiDE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static BlogApiDE.Services.UserServices;

namespace BlogApiDE.Data
{
    public static class DbInitializer
    {

     
        public static void Seed(BlogDbContext context, IUserService userService)
        {
            context.Database.EnsureCreated();

            #region User

            if (!context.Users.Any())
            {
               userService.Create(new User
                {
                    Username = "admin",
                    FirstName = "Quoc",
                    LastName = "Khanh",
                    Email = "tedu.international@gmail.com",
                    Role = "Admin",
                }, "admin");
               
            }

            #endregion User

            #region Category

            if (!context.Categories.Any())
            {
                context.Categories.AddRange(new List<Category>
                {
                    new Category {Name = ".Net Core Core", Description = ".Net Core Core" },

                    new Category {Name = "EF Core", Description = "EF Core" },

                    new Category {Name = "Dapper ORM", Description = "Dapper ORM" },
                    new Category {Name = "NHibernate Framework", Description = "NHibernate Framework" },
                  
                });
               context.SaveChanges();
            }
            #endregion Category

            #region Post
            if (!context.Posts.Any())
            {
                context.Posts.AddRange(new List<Post>
                {
                    new Post {CategoryId=1,Title="Get started with .Net Core",ShortDescription="Create new .Net Core app",FullDescription="Create new .Net Core app", Published=true},

                    new Post {CategoryId=2,Title="Get started with EF Core", ShortDescription="Apply Ef core code first", FullDescription="Apply Ef core code first", Published=true},

                    new Post {CategoryId=3,Title="Get started with Dapper ORM", ShortDescription="Apply Dapper ORM", FullDescription="Apply Dapper ORM", Published=true},

                    new Post {CategoryId=4,Title="Get started with NHibernate Framework", ShortDescription="Apply NHibernate Framework", FullDescription="Apply NHibernate Framework", Published=true},
             

                });
                context.SaveChanges();
            }
            #endregion   Post

        }

     
    }
}
