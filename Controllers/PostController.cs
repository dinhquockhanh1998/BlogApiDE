﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogApiDE.Data;
using BlogApiDE.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BlogApiDE.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private BlogDbContext db;
        public PostController(BlogDbContext context)
        {
            this.db = context;

        }

        #region Posts

        // GET api/room
        [AllowAnonymous]
        [HttpGet("GetAll")]
        public ActionResult<IEnumerable<Post>> GetAll()
        {
            return db.Posts;
        }
        [AllowAnonymous]
        // GET api/values/5
        [HttpGet("Get/{id}")]
        public ActionResult<Post> Get(int id)
        {
            return db.Posts.Where(p => p.Id == id).FirstOrDefault<Post>();
        }


        [AllowAnonymous]
        // POST api/values
        [HttpPost("Create")]
        public bool Create(Post post)
        {
            try
            {

                post.AddedDate = DateTime.Now;
                post.ModifiedDate = DateTime.Now;
                db.Posts.Add(post);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
       
        // PUT api/values/5
        [HttpPut("Put")]
        public bool Put(Post post)
        {
            try
            {
                Post rel = db.Posts.Find(post.Id);
                post.AddedDate = rel.AddedDate;
                post.AddedBy = rel.AddedBy;
                post.ModifiedDate = DateTime.Now;
                db.Entry(rel).CurrentValues.SetValues(post);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        // DELETE api/values/5
        [HttpDelete("Delete")]
        public bool Delete(Post post)
        {
            try
            {
 
                db.Posts.Remove(post);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion Posts

        #region Comments
        [AllowAnonymous]
        [HttpGet("{id}/comments")]
        public async Task<IActionResult> GetComments(int id)
        {
            var query = db.Comments.Where(x => x.PostId == id).AsQueryable();
           
            var items = await query
                .Select(c => new Comment()
                {
                    CommentId = c.CommentId,
                    Content = c.Content,
                    AddDate = c.AddDate,
                    PostId = c.PostId,
                    ModifiedDate = c.ModifiedDate,
                    AddedBy = c.AddedBy
                })
                .ToListAsync();
 
            return Ok(items);
        }

        [HttpGet("{id}/comments/{commentId}")]
        public async Task<IActionResult> GetCommentDetail(int commentId)
        {
            var comment = await db.Comments.FindAsync(commentId);
            if (comment == null)
                return NotFound();

            var item = new Comment()
            {
                CommentId = comment.CommentId,
                Content = comment.Content,
                AddDate = comment.AddDate,
                PostId = comment.PostId,
                ModifiedDate = comment.ModifiedDate,
                AddedBy = comment.AddedBy
            };

            return Ok(item);
        }

        [HttpPost("{id}/comments/PostComment")]
        public async Task<IActionResult> PostComment(int id, Comment request)
        {
            var comment = new Comment()
            {
                Content = request.Content,
                PostId = id,
                AddedBy = request.AddedBy,
                AddDate = DateTime.Now,
                ModifiedDate = DateTime.Now
            };
            db.Comments.Add(comment);


            var result = await db.SaveChangesAsync();
            if (result > 0)
            {
                return CreatedAtAction(nameof(GetCommentDetail), new { id = id, commentId = comment.CommentId }, request);
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut("{id}/comments/PutComment/{commentId}")]
        public async Task<IActionResult> PutComment(int commentId, Comment request)
        {
            var comment = await db.Comments.FindAsync(commentId);
            if (comment == null)
                return NotFound();
            

            comment.Content = request.Content;
            comment.ModifiedDate = DateTime.Now;
            db.Comments.Update(comment);

            await db.SaveChangesAsync();

            return Ok(comment);
        }

        [HttpDelete("{id}/comments/{commentId}")]
        public async Task<IActionResult> DeleteComment(int id, int commentId)
        {
            var comment = await db.Comments.FindAsync(commentId);
            if (comment == null)
                return NotFound();

            db.Comments.Remove(comment);

            var result = await db.SaveChangesAsync();
            if (result > 0)
            {
                var item = new Comment()
                {
                    CommentId = comment.CommentId,
                    Content = comment.Content,
                    AddDate = comment.AddDate,
                    PostId = id,
                    ModifiedDate = comment.ModifiedDate,
                    AddedBy = comment.AddedBy
                };
                return Ok(item);
            }
            return BadRequest();
        }
        #endregion Comments

    }
}