﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogApiDE.Data;
using BlogApiDE.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BlogApiDE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        
        private BlogDbContext db;
        public CategoryController(BlogDbContext context)
        {
            this.db = context;

        }
        // GET api/room
        [AllowAnonymous]
        [HttpGet("GetAll")]
        public ActionResult<IEnumerable<Category>> GetAll()
        {
            return db.Categories;
        }
        [AllowAnonymous]
        // GET api/values/5
        [HttpGet("Get/{id}")]
        public ActionResult<Category> Get(int id)
        {
            return db.Categories.Where(p => p.CategoryId == id).FirstOrDefault<Category>();
        }
        [AllowAnonymous]
        // POST api/values
        [HttpPost("Create")]
        public bool Create(Category category)
        {
            try
            {
                db.Categories.Add(category);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        // PUT api/values/5
        [HttpPut("Put")]
        public bool Put(Category category)
        {
            try
            {
                Category rel = db.Categories.Find(category.CategoryId);
       
                db.Entry(rel).CurrentValues.SetValues(category);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }


        // DELETE api/values/5
        [HttpDelete("Delete")]
        public bool Delete(Category category)
        {
            try
            {

                db.Categories.Remove(category);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}
