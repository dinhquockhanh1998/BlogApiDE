﻿using BlogApiDE.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogApiTestDE
{
    public class InMemoryDbContextFactory
    {
        public BlogDbContext GetBlogDbContext()
        {
            var options = new DbContextOptionsBuilder<BlogDbContext>()
                       .UseInMemoryDatabase(databaseName: "InMemoryApplicationDatabase")
                       .Options;
            var dbContext = new BlogDbContext(options);

            return dbContext;
        }
    }
}
