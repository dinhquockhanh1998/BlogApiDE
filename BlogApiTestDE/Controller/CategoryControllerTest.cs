﻿using BlogApiDE.Controllers;
using BlogApiDE.Data;
using BlogApiDE.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BlogApiTestDE.Controller
{
    public class CategoryControllerTest
    {
        private BlogDbContext _context;
        

        public CategoryControllerTest()
        {
            _context = new InMemoryDbContextFactory().GetBlogDbContext();
       
        }

        [Fact]
        public void Should_Create_Instance_Not_Null_Success()
        {
            var controller = new CategoryController(_context);
            Assert.NotNull(controller);
        }

        [Fact]
        public void GetAll_HasData_ReturnSuccess()
        {
            _context.Categories.AddRange(new List<Category>()
            {
              ( new Category()
                {
                CategoryId = 1,
                Name = "test1",
                Description = "test1"
                })
            });
             _context.SaveChanges();
            var categoryController = new CategoryController(_context);
            var result = categoryController.GetAll();
           
            Assert.NotNull(result);
        }

        [Fact]
        public void GetById_HasData_ReturnSuccess()
        {
            _context.Categories.AddRange(new List<Category>()
            {
              ( new Category()
                {
                CategoryId = 1,
                Name = "test1",
                Description = "test1"
                })
            });
            _context.SaveChanges();
            var categoryController = new CategoryController(_context);
            var result = categoryController.Get(1);

            Assert.NotNull(result);
        }

        [Fact]
        public void PostCategory_ValidInput_ReturnTrue()
        {
            var categoryController = new CategoryController(_context);
            var result =  categoryController.Create(new Category()
            {
                CategoryId = 1,
                Name = "test1",
                Description = "test1"
            });
            
            Assert.True(result);
        }

        [Fact]
        public void PostCategory_ValidInput_ReturnFalse()
        {
            _context.Categories.AddRange(new List<Category>()
            {
                new Category(){
                    CategoryId = 2,
                    Name = "test2",
                    Description = "test2"
                }
            });
             _context.SaveChanges();

            var categoryController = new CategoryController(_context);
            var result = categoryController.Create(new Category()
            {
                CategoryId = 2,
                Name = "test2",
                Description = "test2"
            });

            Assert.False(result, $"du lieu da ton tai");
        }

        [Fact]
        public void PutCategory_ValidInput_ReturnTrue()
        {
            _context.Categories.AddRange(new List<Category>()
            {
                new Category(){
                    CategoryId = 1,
                    Name = "test2",
                    Description = "test2"
                }
            });
            _context.SaveChanges();

            var categoryController = new CategoryController(_context);
            var result = categoryController.Put(new Category()
            {
                CategoryId = 1,
                Name = "test1",
                Description = "test1"
            });

            Assert.True(result);
        }

        [Fact]
        public void PutCategory_ValidInput_ReturnFalse()
        {
            _context.Categories.AddRange(new List<Category>()
            {
                new Category(){
                    CategoryId = 1,
                    Name = "test2",
                    Description = "test2"
                }
            });
            _context.SaveChanges();

            var categoryController = new CategoryController(_context);
            var result = categoryController.Put(new Category()
            {
                CategoryId = 2,
                Name = "test1",
                Description = "test1"
            });

            Assert.False(result, $"du lieu khong ton tai");
        }

        [Fact]
        public void DeleteCategory_ValidInput_ReturnTrue()
        {
            _context.Categories.AddRange(new List<Category>()
            {
                new Category(){
                    CategoryId = 1,
                    Name = "test1",
                    Description = "test1"
                },
                new Category(){
                    CategoryId = 2,
                    Name = "test2",
                    Description = "test2"
                },
                new Category(){
                    CategoryId = 3,
                    Name = "test3",
                    Description = "test3"
                },
            });
            _context.SaveChanges();

            var categoryController = new CategoryController(_context);
            var result = categoryController.Delete(new Category()
            {
                CategoryId = 3,
                Name = "test3",
                Description = "test3"
            });

            Assert.True(result);
        }

        [Fact]
        public void DeleteCategory_ValidInput_ReturnFalse()
        {
            _context.Categories.AddRange(new List<Category>()
            {
                new Category(){
                    CategoryId = 1,
                    Name = "test2",
                    Description = "test2"
                }
            });
            _context.SaveChanges();

            var categoryController = new CategoryController(_context);
            var result = categoryController.Delete(new Category()
            {
                CategoryId = 2,
               
            });

            Assert.False(result, $"du lieu khong ton tai");
        }

    }
}
