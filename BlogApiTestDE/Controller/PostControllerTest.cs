﻿using BlogApiDE.Controllers;
using BlogApiDE.Data;
using BlogApiDE.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace BlogApiTestDE.Controller
{
    public class PostControllerTest
    {
        private BlogDbContext _context;


        public PostControllerTest()
        {
            _context = new InMemoryDbContextFactory().GetBlogDbContext();

        }

        [Fact]
        public void Should_Create_Instance_Not_Null_Success()
        {
            var controller = new PostController(_context);
            Assert.NotNull(controller);
        }

        [Fact]
        public void GetAll_HasData_ReturnSuccess()
        {
            _context.Posts.AddRange(new List<Post>()
            {
              ( new Post()
                {
                Id = 1,
                CategoryId = 1,
                Title = "test1",
                ShortDescription = "test1",
                FullDescription = "test1"
                })
            });
            _context.SaveChanges();
            var postController = new PostController(_context);
            var result = postController.GetAll();

            Assert.NotNull(result);
        }

        [Fact]
        public void GetById_HasData_ReturnSuccess()
        {
            _context.Posts.AddRange(new List<Post>()
            {
              ( new Post()
                {
                 Id = 1,
                CategoryId = 1,
                Title = "test1",
                ShortDescription = "test1",
                FullDescription = "test1"
                })
            });
            _context.SaveChanges();
            var postController = new PostController(_context);
            var result = postController.Get(1);

            Assert.NotNull(result);
        }

        [Fact]
        public void CreatePost_ValidInput_ReturnTrue()
        {
            var PostController = new PostController(_context);
            var result = PostController.Create(new Post()
            {
                Id = 1,
                CategoryId = 1,
                Title = "test1",
                ShortDescription = "test1",
                FullDescription = "test1"
            });

            Assert.True(result);
        }

        [Fact]
        public void CreatePost_ValidInput_ReturnFalse()
        {
            _context.Posts.AddRange(new List<Post>()
            {
                new Post(){
                Id = 1,
                CategoryId = 1,
                Title = "test1",
                ShortDescription = "test1",
                FullDescription = "test1"
                }
            });
            _context.SaveChanges();

            var PostController = new PostController(_context);
            var result = PostController.Create(new Post()
            {
                Id = 1,
                CategoryId = 1,
                Title = "test1",
                ShortDescription = "test1",
                FullDescription = "test1"
            });

            Assert.False(result, $"du lieu da ton tai");
        }

        [Fact]
        public void PutPost_ValidInput_ReturnTrue()
        {
            _context.Posts.AddRange(new List<Post>()
            {
                new Post(){
                Id = 1,
                CategoryId = 1,
                Title = "test1",
                ShortDescription = "test1",
                FullDescription = "test1"
                }
            });
            _context.SaveChanges();

            var PostController = new PostController(_context);
            var result = PostController.Put(new Post()
            {
                Id = 1,
                CategoryId = 2,
                Title = "test2",
                ShortDescription = "test2",
                FullDescription = "test2"
            });

            Assert.True(result);
        }

        [Fact]
        public void PutPost_ValidInput_ReturnFalse()
        {
            _context.Posts.AddRange(new List<Post>()
            {
                new Post(){
                Id = 1,
                CategoryId = 1,
                Title = "test1",
                ShortDescription = "test1",
                FullDescription = "test1"
                }
            });
            _context.SaveChanges();

            var PostController = new PostController(_context);
            var result = PostController.Put(new Post()
            {
                Id = 2,
                CategoryId = 2,
                Title = "test2",
                ShortDescription = "test2",
                FullDescription = "test2"
            });

            Assert.False(result, $"du lieu khong ton tai");
        }

        [Fact]
        public void DeletePost_ValidInput_ReturnTrue()
        {
            _context.Posts.AddRange(new List<Post>()
            {
                new Post(){
                Id = 1,
                CategoryId = 1,
                Title = "test1",
                ShortDescription = "test1",
                FullDescription = "test1"
                },
                new Post(){
                Id = 2,
                CategoryId = 2,
                Title = "test2",
                ShortDescription = "test2",
                FullDescription = "test2"
                },
                new Post(){
                Id = 3,
                CategoryId = 3,
                Title = "test3",
                ShortDescription = "test3",
                FullDescription = "test3"
                },
            });
            _context.SaveChanges();

            var PostController = new PostController(_context);
            var result = PostController.Delete(new Post()
            {
                Id = 1,
                CategoryId = 1,
                Title = "test1",
                ShortDescription = "test1",
                FullDescription = "test1"
            });

            Assert.True(result);
        }

        [Fact]
        public void DeletePost_ValidInput_ReturnFalse()
        {
            _context.Posts.AddRange(new List<Post>()
            {
                new Post(){
                Id = 1,
                CategoryId = 1,
                Title = "test1",
                ShortDescription = "test1",
                FullDescription = "test1"
                }
            });
            _context.SaveChanges();

            var PostController = new PostController(_context);
            var result = PostController.Delete(new Post()
            {
                Id = 2,
                CategoryId = 1,
                Title = "test1",
                ShortDescription = "test1",
                FullDescription = "test1"

            });

            Assert.False(result, $"du lieu khong ton tai");
        }

    }
}
