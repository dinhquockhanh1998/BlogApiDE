﻿using AutoMapper;
using BlogApiDE.Controllers;
using BlogApiDE.Data;
using BlogApiDE.Dtos;
using BlogApiDE.Helpers;
using BlogApiDE.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using static BlogApiDE.Services.UserServices;

namespace BlogApiTestDE.Controller
{
    public class UserControllerTest
    {
        private Mock<IUserService> _mockUserService;
        private Mock<IMapper> _mockMapper;
        private readonly Mock<IOptions<AppSettings>> _mockAppSettings;

        public UserControllerTest()
        {
            _mockUserService = new Mock<IUserService>();
            _mockMapper = new Mock<IMapper>();
            _mockAppSettings = new Mock<IOptions<AppSettings>>();
        }

        [Fact]
        public void Should_Create_Instance_Not_Null_Success()
        {
            var controller = new UserController(_mockUserService.Object, _mockMapper.Object, _mockAppSettings.Object);
            Assert.NotNull(controller);
        }

        [Fact]
        public void Authenticate_ValidInput_ReturnTrue()
        {
            _mockUserService.Setup(x => x.Create(new User
            {
                Username = "admin",
                FirstName = "Quoc",
                LastName = "Khanh",
                Email = "tedu.international@gmail.com",
                Role = "Admin",
            }, "admin"));

            _mockUserService.Setup(x => x.Authenticate("admin", "admin"));
               
            var userController = new UserController(_mockUserService.Object, _mockMapper.Object, _mockAppSettings.Object);
            var result = userController.Authenticate(new AuthenticateDto()
            {
                Username = "admin",
                Password = "admin"
            });
            Assert.NotNull(result);
            Assert.IsType<OkResult>(result);
        }
    }
}
